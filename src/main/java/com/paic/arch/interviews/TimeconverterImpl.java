package com.paic.arch.interviews;

import java.util.Arrays;
import java.util.Collections;

/**
 * 描述:
 *
 * @author limulin
 * @version v1.0.0
 * @since v1.0.0
 * created on 2018/3/2
 */

public class TimeconverterImpl implements TimeConverter {
    @Override
    public String convertTime(String aTime) {

        //第一步：前置断言，首先检查aTime时间参数的有效性，这里只做简单检查，后期可添加详细的校验，如时间格式等
        if(aTime == null) {
            throw new IllegalArgumentException("时间字符串为空错误");
        }

        //第二步：将时间字符串按“：”（冒号）切分，为后续取出时、分、秒做准备
        String[] times = aTime.split(":", 3);

        int hours=0;
        int minutes=0;
        int seconds = 0;

        try {
            //分别取出时、分、秒
            hours = Integer.parseInt(times[0]);
            minutes = Integer.parseInt(times[1]);
            seconds = Integer.parseInt(times[2]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("时分秒字符串转整数时异常");
        }


        /**
         * 第三步：以下根据时钟的每一行的不同情况进行处理
         * 第1行，每两秒亮灯：seconds % 2
         * 第2行，每亮一盏灯代表5个小时：hours / 5 向下取整
         * 第3行，每亮一盏灯代表1个小时：hours % 5 取模
         * 第4行，每亮一盏灯代表15分钟，并且如果有连续三个Y（黄灯），则最后一个是R（红灯），用正则表达式进行查找匹配后替换：minutes / 5 向下取整
         * 第5行，每亮一盏灯代表1分钟：minutes % 5 取模
         *
         */
        String secondsLine1 = (seconds % 2 == 0) ? "Y" : "O";
        String hoursLine2 = timeDisplayForRow(4, hours / 5, "R");
        String hoursLine3 = timeDisplayForRow(4, hours % 5, "R");
        String minutesLine4 = timeDisplayForRow(11, minutes / 5, "Y").replaceAll("YYY", "YYR");
        String minutesLine5 = timeDisplayForRow(4, minutes % 5, "Y");

        //第四步：也是最后一步，将以上每一行连接起来
        return String.join(System.getProperty("line.separator"), Arrays.asList(secondsLine1, hoursLine2, hoursLine3, minutesLine4, minutesLine5));
    }

    /**
     * 根据一行的实际情况，实现该行的亮灯情况.
     * @param lightAmount 一行中，灯的总数量
     * @param lightIsUpAmount 灯是亮着的总数
     * @param lightColorType 灯亮着时的颜色类型
     * @returnn 返回一个代表该行亮灯情况的字符串.
     */
    private String timeDisplayForRow(int lightAmount, int lightIsUpAmount, String lightColorType) {

        //该行中灯是灭着的的总数
        int lightIsOffAmount = lightAmount - lightIsUpAmount;
        String lightIsUpString = String.join("", Collections.nCopies(lightIsUpAmount, lightColorType));
        String lightIsOffString = String.join("", Collections.nCopies(lightIsOffAmount, "O"));

        //拼接该行中灯亮与不亮的情况
        return lightIsUpString + lightIsOffString;
    }
}
